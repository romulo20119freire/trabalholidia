const express = require('express');
const reservas = require('../model/reserva');
const router = express.Router();
router.delete('/delete/:id', async (req, res) => {
    try{
        const reservado = await reservas.deleteOne({_id: req.params.id});
        return res.json({ message: "Deletado com sucesso" });
    } catch (err) {
        return res.status(400).send({ error: 'Falha na tentativa de deletar a reserva!'});
        
    }
});



 


module.exports = app => app.use('/e', router);