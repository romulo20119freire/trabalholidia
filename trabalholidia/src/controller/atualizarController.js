const express = require('express');
const reservas = require('../model/reserva');
const router = express.Router();
router.put('/atualizar/:id', async (req, res) => {
    try{
        reservas.update({_id:req.params.id },req.body, (error, result)=>{
            if(error) res.json(error);
            res.json(result);
        });
    } catch (err) {
        return res.status(400).send({ error: 'Falha na tentativa de efetuar a reserva!'});
        
    }
});


 


module.exports = app => app.use('/d', router);