const express = require('express');
const reservas = require('../model/reserva');
const router = express.Router();
router.post('/reservar', async (req, res) => {
    try{
        const reservado = reservas.create(req.body);
        return res.json({ message: "Reserva efetuada confira no banco de dados..." });
    } catch (err) {
        return res.status(400).send({ error: 'Falha na tentativa de efetuar a reserva!'});
        
    }
});



 


module.exports = app => app.use('/catch', router);