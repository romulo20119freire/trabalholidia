const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/reservaSala', { useMongoClient: true });
mongoose.Promise = global.Promise;
module.exports = mongoose;