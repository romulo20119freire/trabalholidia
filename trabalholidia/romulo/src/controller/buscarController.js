const express = require('express');
const reservas = require('../model/reserva');
const router = express.Router();
router.get('/buscar', async (req, res) => {
    try{
        reservas.find({ }, (error, result)=>{
            if(error) res.json(error);
            res.json(result);
        });
    } catch (err) {
        return res.status(400).send({ error: 'Falha na tentativa de efetuar a reserva!'});
        
    }
});


 


module.exports = app => app.use('/c', router);