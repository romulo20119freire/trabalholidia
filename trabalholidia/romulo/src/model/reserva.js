const mongoose = require('../database');

const reservaSchema = new mongoose.Schema({
    idTecAdm: {type: Number, require: true},
    idDocente: {type: Number, requeire: true},
    numUnidade: {type: Number, require: true},
    numBloco: {type: Number, require: true},
    numSala: {type: Number, require: true},
    dataUsoSala: {type: Date},
    periodo: {type: String, require: true},
    dataPedido: {type: Date, default: Date.now}
});
const reservas = mongoose.model('reservas', reservaSchema);
module.exports = reservas;